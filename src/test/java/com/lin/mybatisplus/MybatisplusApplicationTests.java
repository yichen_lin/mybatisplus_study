package com.lin.mybatisplus;

import com.lin.mybatisplus.entity.User;
import com.lin.mybatisplus.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MybatisplusApplicationTests {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void testupdate(){
        User user = userMapper.selectById(9);
        user.setName("zhangsan");
        int count = userMapper.updateById(user);
        System.out.println(count);
    }
    @Test
    public void testdelete(){
        int count  = userMapper.deleteById(8);
        System.out.println(count);
    }

    @Test
    public void testAdd(){
        User user = new User();
        user.setName("linhonzhou");
        user.setAge(21);
        int count = userMapper.insert(user);
        System.out.println(count);
    }
    @Test
    public void findAll(){
        List<User> user = userMapper.selectList(null);
        System.out.println(user);
    }
}
