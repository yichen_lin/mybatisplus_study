package com.lin.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lin.mybatisplus.entity.User;

public interface UserService extends IService<User> {
}
