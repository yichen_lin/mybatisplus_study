package com.lin.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
public class User {
    @TableId(type = IdType.AUTO)
    private int id;
    private String name;
    private int age;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill  = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    @TableLogic
    @TableField(fill = FieldFill.INSERT)
    private int deleted;
    @Version
    @TableField(fill = FieldFill.INSERT)
    private int version;
}
