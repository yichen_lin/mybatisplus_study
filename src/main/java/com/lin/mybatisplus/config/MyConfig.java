package com.lin.mybatisplus.config;

import com.baomidou.mybatisplus.extension.plugins.OptimisticLockerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@MapperScan("com.lin.mybatisplus.mapper")
@Configuration
public class MyConfig {

    @Bean
    public OptimisticLockerInterceptor f(){
        return new OptimisticLockerInterceptor();
    }
}
