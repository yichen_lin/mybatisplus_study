package com.lin.mybatisplus.controller;

import com.lin.mybatisplus.entity.User;
import com.lin.mybatisplus.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/controller")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/findAll")
    public List<User> findAllUser(){
        List<User> list = userService.list();
        return list;
    }
    //逻辑删除用户设置
    @GetMapping("{id}")
    public boolean deleteByid(@PathVariable int id){
        boolean flag = userService.removeById(id);
        return flag;


    }
}
